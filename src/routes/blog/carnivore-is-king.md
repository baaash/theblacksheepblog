---
title: Carniovore is King!
author: Cooper Oscarfono
date: '2024-04-29'
categories:
- "opinion"
- "controversial"
- "health"
- "nutrition"

image: '/content-images/meat.svg'
alt: 'meaty crown'
published: true
---
